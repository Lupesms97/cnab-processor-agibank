# Projeto Spring Batch - Desafio

Este repositório contém um projeto Spring Batch desenvolvido como parte de um desafio. O objetivo do desafio era aprender os conceitos do Spring Batch em um dia e implementá-los em um projeto.
A ideia do projeto é processar um arquivo de entrada CNAB e gerar um relatório de saída com informações consolidadas.

## Para acessar a interface do Front-end

[Front-end AgiBank](https://agibank-springbatch.web.app/batch)

## Repositório do Desafio

O código-fonte do desafio pode ser encontrado no seguinte repositório:

[Repositório do Desafio Spring Batch - PagNet](https://github.com/Pagnet/desafio-back-end?tab=readme-ov-file)

## Estrutura do Projeto

Os arquivos de teste para o projeto estão localizados na pasta `filesToTest` do repositório. Eles são destinados a verificar a funcionalidade e a integridade do Spring Batch.

## Espaço para Melhorias

Dado o prazo restrito para aprender e implementar os conceitos do Spring Batch, há espaço para melhorias significativas no projeto. Algumas áreas para considerar incluem:

- Higienização dos dados de entrada.
- Refatoração do código para seguir melhores práticas de design e desenvolvimento.
- Adição de testes para cobrir diferentes cenários e garantir robustez.
- Implementação de recursos avançados do Spring Batch, como particionamento, retry policies, escalabilidade etc.
- Melhorias na configuração e gerenciamento de jobs e steps.
- Integração com outras tecnologias e sistemas, conforme necessário para os requisitos do projeto.
